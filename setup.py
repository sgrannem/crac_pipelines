#!/usr/bin/python

__author__      = "Sander Granneman"
__copyright__   = "Copyright 2021"
__version__     = "0.0.1"
__credits__     = ["Sander Granneman"]
__maintainer__  = ["Sander Granneman"]
__email__       = "Sander.Granneman@ed.ac.uk"
__status__      = "beta"

import sys

try:
	from setuptools import setup
	from setuptools.command import easy_install
	sys.stdout.write("Python development and setuptools have been installed...\n")
except:
	sys.stderr.write("Python development and setuptools have not been installed on this machine\nPlease contact the admin of this computer to install these modules\n")
	exit()

setup(name='CLASH_Chimera_Pipeline',
	version='%s' % __version__,
	description='Python code for analysing RNA-binding domain mass-spec data.',
	author='Sander Granneman',
	author_email='Sander.Granneman@ed.ac.uk',
	packages=['CLASH_Chimera_Pipeline',
			  'CLASH_Chimera_Pipeline.Scripts'],
	install_requires=['selenium >= 3.141.0',
					  'argparse >= 1.1',
					  'regex >= 2.5.84',
					  'biopandas >= 0.2.7',
					  'pathlib',
					  'pypdb',
					  'biopandas'	,
					  'pyopenms',
					  ],
	scripts=[
					'pyRBDome/Scripts/aaRNA_submit.py',
					'pyRBDome/Scripts/BindUP_submit.py',
					'pyRBDome/Scripts/Consurf_submit.py',
					'pyRBDome/Scripts/RNABindRPlus_submit.py',
					'pyRBDome/Scripts/STP_submit.py',
					'pyRBDome/Scripts/FTMap_submit.py',
					'pyRBDome/Scripts/GetSequencesFromPDB.py'
				],
	classifiers=[   'Development Status :: 5 - Production/Stable',
					'Environment :: Console',
					'Intended Audience :: Education',
					'Intended Audience :: Developers',
					'Intended Audience :: Science/Research',
					'License :: Freeware',
					'Operating System :: MacOS :: MacOS X',
					'Operating System :: POSIX',
					'Programming Language :: Python :: 3.6',
					'Topic :: Scientific/Engineering :: Bio-Informatics',
					'Topic :: Software Development :: Libraries :: Application Frameworks'
				]
		)
