# CRAC sequencing data processing pipeline:

## Contents

- [Overview](#overview)
- [Repo Contents](#repo-contents)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Results](#results)
- [License](./LICENSE.txt)
- [Citation](#citation)

## Overview
This is a repository containing the scripts for the single-end (SE) and paired-end (PE) RNA sequencing pipelines.
This pipeline is routinely used by our lab to process single-and paired-end CRAC datasets as well as RNA-sequencing datasets.
The pipeline does the following steps:

1) Uses pyBarcodeFilter (pyCRAC package) to demultiplex the data using in-read barcode sequences (optional step)
2) Trims the adapters (optional step)
3) Maps the reads to the reference genome using Novoalign
4) Generates a hittable (list of how many reads mapped to genomic features) using pyReadCounters (pyCRAC package)
5) Generates bedgraph files (normalised to reads per million) for visualising the data in genome browsers (pyCRAC package)

## Repo Contents

- [CRAC_pipeline_PE.py](./CRAC_pipeline_PE.py): Pipeline for processing paired-end CRAC data
- [CRAC_pipeline_SE.py](./CRAC_pipeline_SE.py): Pipeline for processing single-end CRAC data
- [tests](./tests): Jupyter notebook for running the pipeline code

The tests folder contains a small number of input files that you can use to test the code.
The notebook provides examples of how to run the code so that you can use it for your own data.

## System Requirements

### Hardware Requirements

RAM: 16+ GB  
CPU: At least four cores are recommended.

The runtimes vary considerably, depending on how large the sequencing data files are.
But a typical run generally takes several hours to complete.

### Software requirements

Linux: Ubuntu 18.04 and higher

Other dependencies:

=======
 - Python 3.6+ or higher
 - ruffus 2.6 or higher (http://www.ruffus.org.uk)
 - pyCRAC 1.5.1 or higher (https://git.ecdf.ed.ac.uk/sgrannem/pycrac)
 - flexbar 3.5 or higher (https://github.com/seqan/flexbar)
 - novoalign 2.0 or higher (http://www.novocraft.com/products/novoalign/)

## Installation Guide

Simply copy the scripts to the location where your data is stored or copy it into a folder that is in the system path.
Make sure to run:

```
chmod a+x CRAC_pipeline_PE.py
chmod a+x CRAC_pipeline_SE.py
```

## Results

Each script has a detailed help menu. To access this type the following:

```
python CRAC_pipeline_PE.py -h
```

To run the code on the test data you can try the following on the test files.
This runs the software on 8 cores and returns the results in the output folder 'test_run'

```
python CRAC_pipeline_PE.py \
-f test_data/test_1.fastq \
-r test_data/test_2.fastq \
--name test_run \
--novoindex genome_files/Staphylococcus_aureus_usa300_FPR3757_corrected.novoindex \
--gtf genome_files/Staphylococcus_aureus_usa300_FPR3757_1.3_with_intergenic.gtf \
-c genome_files/Staphylococcus_aureus_usa300_FPR3757_corrected.length \
-a adapter_sequences/three_prime_adapters.fasta \
-a2 adapter_sequences/five_adapters_reverse_complemented.fa \
-p 8 &
```

```
python CRAC_pipeline_SE.py \
-f test_data/test_1.fastq \
--name test_single_end_run \
--novoindex ./genome_files/Staphylococcus_aureus_usa300_FPR3757_corrected.novoindex \
--gtf genome_files/Staphylococcus_aureus_usa300_FPR3757_1.3_with_intergenic.gtf \
-c genome_files/Staphylococcus_aureus_usa300_FPR3757_corrected.length \
-a adapter_sequences/three_prime_adapters.fasta \
-p 8 &
```

on the scripts to make them executable on your machine.

## Citation

For usage of the package and associated manuscript, please cite according to the enclosed [citation.bib](./citation.bib).
